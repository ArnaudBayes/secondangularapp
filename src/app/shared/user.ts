export class User {
    private _lastName: string;
    public get lastName(): string {
        return this._lastName;
    }
    public set lastName(value: string) {
        this._lastName = value;
    }
    private _name: string;
    public get name(): string {
        return this._name;
    }
    public set name(value: string) {
        this._name = value;
    }
    private _date: Date;
    public get date(): Date {
        return this._date;
    }
    public set date(value: Date) {
        this._date = value;
    }
    private _formation: string;
    public get formation(): string {
        return this._formation;
    }
    public set formation(value: string) {
        this._formation = value;
    }

   /**
    * User
    */
   public User(lastname?:string, name?:string, date?:Date, formation?:string) {
       this._lastName=lastname;
       this._name=name;
       this._date=date;
       this._formation=formation;
       
   }

}
