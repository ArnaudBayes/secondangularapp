import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Router } from '@angular/router';
import { CatalogueService } from '../catalogue/catalogue.service';
import { Products } from '../catalogue/catalogue';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

 private _listProduct: Array<Products>= new Array<Products>();
 private selectedId : number;
 private selectedProduct: Products;
 private position: number;

  constructor(private _route: ActivatedRoute, private _router: Router, private _serviceCatalogue: CatalogueService) {
    console.log(this._route.snapshot.paramMap.get('id'));
    console.log("Liste des prods chargés: ", this._serviceCatalogue._listProduct);
    console.log("taille du tableau: ", this._serviceCatalogue._listProduct.length);
    this._listProduct= this._serviceCatalogue._listProduct;
    this.selectedId=parseInt(this._route.snapshot.paramMap.get('id'));

    this._route.paramMap.subscribe((paramsData : ParamMap) => {
      console.log("Paramètre récupérés : =>", paramsData);
      this.selectedId = parseInt(paramsData.get('id'));
      
    this._listProduct.forEach(element => {
      if (element.productId==this.selectedId)
       this.selectedProduct=element;
      });
      this.position= this._listProduct.indexOf(this.selectedProduct);
    })

   }

   onBack(): void {
     this._router.navigate(['/catalogue'])
   }


   goPrevious() {
     if (this.position != 0)
     {
     let prev = this._listProduct[this.position-1].productId;
     this._router.navigate(['/catalogue', prev]);
     }
     else
     {
      let prev =  this.selectedId;
      this._router.navigate(['/catalogue', prev]);
     }
   }

    goNext() {
      if (this.position < this._listProduct.length-1)
      {       
      let prev = this._listProduct[this.position+1].productId;
      this._router.navigate(['/catalogue', prev]);
      }
      else 
      {
        let prev =this.selectedId;
        this._router.navigate(['/catalogue', prev]); 
      }
    }
  
    


  ngOnInit() {
  }

}
