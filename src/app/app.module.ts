import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import{ FormsModule} from '@angular/forms';
import { CatalogueComponent } from './catalogue/catalogue.component';
import { FilterPipe } from './shared/filter.pipe';
import { HomeComponent } from './home/home.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import{ HttpClientModule } from '@angular/common/http';
import{ CatalogueService } from './catalogue/catalogue.service';
import { ProductDetailComponent } from './product-detail/product-detail.component';



@NgModule({
  declarations: [
    AppComponent,
    CatalogueComponent,
    FilterPipe,
    HomeComponent,
    ProductDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    HttpClientModule,
    
  ],
  providers: [CatalogueService],
  bootstrap: [AppComponent]
})
export class AppModule { }
