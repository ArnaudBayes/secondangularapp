import { Component } from '@angular/core';
import { User } from './shared/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'SecondAngularApp';

  tmpUser: User = new User();
  listUser: Array<User>= new Array<User>();

  enregister(){
    if(this.tmpUser.lastName.length>0 && this.tmpUser.name.length>0)
    {
      this.listUser.push(this.tmpUser);
      this.tmpUser=new User();
      console.log(this.listUser)
    }
  }
supprimer(i){
   this.listUser.splice(i,1);
   console.log(i);
}
  
modifier(item ){ 
this.tmpUser=item;
}
}
