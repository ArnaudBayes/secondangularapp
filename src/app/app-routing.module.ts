import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogueComponent } from './catalogue/catalogue.component';
import { HomeComponent } from './home/home.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';

const routes: Routes = [
 {path: 'home', component : HomeComponent},
 {path: 'catalogue', component: CatalogueComponent},
 {path: 'catalogue/:id', component: ProductDetailComponent},
 {path: '', redirectTo: 'home', pathMatch: 'full'},
 {path: '**', redirectTo: 'home', pathMatch: 'full'}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
