import{ Injectable } from '@angular/core';
import{ HttpClient } from '@angular/common/http';
import { Products } from './catalogue';

@Injectable({
  providedIn: 'root'
})
export class CatalogueService {
  private _productUrl = '../../assets/api/products/products.json';
  private _postUrl = 'https://jsonplaceholder.typicode.com/posts';

  public _listProduct : Array<Products>= new Array<Products>();

  constructor(private _http: HttpClient) { }

  getPosts(){
  return this._http.get(this._postUrl);
  }

  getCatalogue(){
  return this._http.get(this._productUrl);
  }

  

 
}
