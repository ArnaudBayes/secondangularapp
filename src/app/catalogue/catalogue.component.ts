import { Component, OnInit } from '@angular/core';
import { CatalogueService } from './catalogue.service';
import { Products } from './catalogue';


@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss']
})
export class CatalogueComponent implements OnInit {
  buttonTitle = "Hide Image";
  showImage=true;
  searchText: string ="";
  listProducts : Array<Products>= new Array<Products>();

  
  constructor(private _serviceCatalogue: CatalogueService) {

    this._serviceCatalogue.getCatalogue().subscribe(data=>{
      console.log("============ Les données reçues du WS : =================", data)
      this.listProducts = data as Products[];
      this._serviceCatalogue._listProduct=this.listProducts;

     });
   }

   
  ngOnInit() {
  }

  updateImage(){
    if(this.showImage){
      this.buttonTitle = "Show image";
      this.showImage = false;
    }else{
      this.buttonTitle = "Hide image";
      this.showImage = true;
    }
  }
}